# Digits recognition researching #

The goal is using [Keras](https://keras.io/) build pipeline for training and using NN for the digits recognition. A first part is 
creating dataset of the written numbers and using augmentation for increase this dataset. Right now I used simple 
convolutional neural network. The accuracy of the model for the testing dataset is 98%.

## Installation

For installation please use [conda](http://conda.pydata.org/docs/using/index.html). 

Setup environment:
```sh
conda env create -f environment.yml
```
After the installation was done successfully, activate your enviroment:
```sh
source activate digits
```
## Usage

After activate source, run `jupyter notebook digits_recognition.ipynb`.